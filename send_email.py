import os
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from typing import Optional

def send_email(body: str, subject: str, recipient: str) -> None:
    smtp_server = os.environ.get('SMTP_SERVER')
    sender_email = os.environ.get('SENDER_EMAIL')
    password = os.environ.get('EMAIL_PASSWORD')

    if not smtp_server or not sender_email or not password:
        raise ValueError('SMTP server, sender email, and password must be set in environment variables.')

    message = MIMEMultipart()
    message['From'] = sender_email
    message['To'] = recipient
    message['Subject'] = subject

    message.attach(MIMEText(body, 'plain'))

    try:
        with smtplib.SMTP(smtp_server, 587) as server:
            server.starttls()
            server.login(sender_email, password)
            server.sendmail(sender_email, recipient, message.as_string())
    except Exception as e:
        print(f'Error sending email: {e}')

if __name__ == '__main__':
    body = 'This is a sample email message.'
    subject = 'Sample Email Subject'
    recipient = 'recipient@example.com'
    send_email(body, subject, recipient)